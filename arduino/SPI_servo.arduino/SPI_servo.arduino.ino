#include <Servo.h>
#include <Wire.h>

Servo myservo;

#define SLAVE_ADD 0x08
#define SERVO   9
#define LED_PIN  13
#define BUTTON_PIN  2

volatile int mode = 0;
int prevMode = 0;
int servoPos = 0;
int targetServoPos = 0;
long currentMillis = 0;
long previousMillis = 0;
int interval = 10;

bool button_pushed = LOW;

void setup()
{

  myservo.attach(SERVO);

  Wire.begin(SLAVE_ADD);
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestEvent);
  Serial.begin(9600);
  previousMillis = millis();

  pinMode(LED_PIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  //attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), buttonInterrupt, RISING);
}

void loop()
{
   

  currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;

    if (servoPos < targetServoPos) {
      servoPos += 5;
    }
    else if (servoPos > targetServoPos) {
      servoPos -= 5;
    }
    
    if (servoPos < 30 && targetServoPos < 30){
    	myservo.detach();
    }else if (!myservo.attached()){
    	myservo.attach(SERVO);
    	servoPos=30;
    	myservo.write(servoPos);
    }else{
    	myservo.write(servoPos);
    }
  }
}

// function that e192.168.254.125xecutes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany)
{
  int inData = Wire.read(); // receive byte as an integer
  Serial.println(inData); // print the integer
  targetServoPos = inData;
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent()
{
  Wire.write(button_pushed);
}

void buttonInterrupt() {
      button_pushed = !button_pushed;
      digitalWrite(LED_PIN, button_pushed);
      targetServoPos = 30;
    
}
