# Catbowl Guardian

This project has a accompanying blog post (with hungry cat pictures) at https://portfolio.digitalkip.com/?p=1580

## Resources
We follow the guidance given by the following two articles:

1: https://tutorial.cytron.io/2018/10/17/i2c-raspberry-pi-arduino-controlling-servo-motor/

2: https://learn.sparkfun.com/tutorials/raspberry-pi-spi-and-i2c-tutorial

3: some non-specific tutorial on setting up a unit file for Debian to install this project as a service . 


## Cheatsheet Notes for dean.. 
ssh into pi in terminal
	
	ssh pi@192.168.0.33 

ssh into pi in file window:
	
	ssh://192.168.0.33

/home/pi/startup
	
	update file (copy over ble_gimpy_scanner.py)

rerun service:
	
	sudo systemctl stop gimpy
	sudo systemctl start gimpy

get log of all things printed
	
	tail -f gimpy.log
