#!/bin/python3
# INSTALLTION INSTRUCTIONS
# 1. install bluepy
# 2. install e-paper libraries
# 3. run the setup.py for the e-paper
# 4. configure this script as a service in /lib/systemd/system/gimpy.service
#	https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/

######################################
#				#
#	E-Paper Imports	#
#				#
######################################
# https://www.waveshare.com/2.13inch-e-paper-hat.htm
import sys
import os
picdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'pic')
libdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'lib')
if os.path.exists(libdir):
    sys.path.append(libdir)
#import logging
from waveshare_epd import epd2in13b_V3
import time
from PIL import Image,ImageDraw,ImageFont
import traceback
font20 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 20)
font15 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 15)
epd = epd2in13b_V3.EPD()

######################################
#				#
#	BLE Scanning Library	#
#				#
######################################
from bluepy.btle import Scanner, DefaultDelegate

######################################
#				#
#	IP Address Fetching	#
#				#
######################################
# https://stackoverflow.com/questions/24196932/how-can-i-get-the-ip-address-from-a-nic-network-interface-controller-in-python
import socket
import fcntl
import struct

######################################
#                               #
#         SPI to Arduino        #
#                               #
######################################
import smbus
######################################
#                               #
#       IP Address Fetching     #
#                               #
######################################
i2c = smbus.SMBus(1)
I2C_ADD = 0x08 # Arduino I2C address

#GIMPY='d1:eb:ed:7a:a4:64'
GIMPY='dc:8a:96:70:c2:00' # Tile first startup

IS_CLOSE = 50
IS_FAR   = 70
DEBUG=False
raw_distances=[]
GIMPY_STATUS = 4
LAST_STATUS = 0
IP=''
NOT_fOUND_LOOPS=0

STATUS_1 = "Gimpy is CLOSE"
STATUS_2 = "Gimpy is LURKING"
STATUS_3 = "Gimpy is FAR"
STATUS_4 = "Gimpy is NOT FOUND"

def getIP():
	global IP
	ifname = 'wlan0'	
	
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		IP = str(socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', bytes(ifname[:15], 'utf-8')))[20:24]))
	except:
		IP='OFFLINE'
	return IP

# Draw a line on the e-Paper HAT
# line must be between 1-6
# text is text.
# clear = True means we re-draw the whole thing.  
def draw(text):
	HBlackimage = Image.new('1', (epd.height, epd.width), 255)  # 298*126
	HRYimage = Image.new('1', (epd.height, epd.width), 255)  # 298*126  ryimage: red or yellow image
	drawblack = ImageDraw.Draw(HBlackimage)
	drawry = ImageDraw.Draw(HRYimage)
	drawblack.text((10, 0), 'IP: ' + getIP(), font = font15, fill = 0)
	drawblack.text((10, 30), text, font = font20, fill = 0)
	epd.display(epd.getbuffer(HBlackimage), epd.getbuffer(HRYimage))
	
def initDisplay():
	epd.init()
	epd.Clear()
	time.sleep(1)
	# Drawing on the Horizontal image
	print("Draw the e-Paper default screen...") 
	
	HBlackimage = Image.new('1', (epd.height, epd.width), 255)  # 298*126
	HRYimage = Image.new('1', (epd.height, epd.width), 255)  # 298*126  ryimage: red or yellow image  
	drawblack = ImageDraw.Draw(HBlackimage)
	drawry = ImageDraw.Draw(HRYimage)
	drawblack.text((10, 0), 'Gimpy Food Blocker', font = font20, fill = 0)
	drawblack.text((10, 30), 'Initializing...', font = font20, fill = 0)
	#drawblack.text((120, 0), u'微雪电子', font = font20, fill = 0)    
	#drawblack.line((20, 50, 70, 100), fill = 0)
	#drawblack.line((70, 50, 20, 100), fill = 0)
	#drawblack.rectangle((20, 50, 70, 100), outline = 0)
	epd.display(epd.getbuffer(HBlackimage), epd.getbuffer(HRYimage))
	#epd.init()
	#epd.Clear()
	time.sleep(1)

def writeI2C(data):
  i2c.write_byte(I2C_ADD, data)

def readI2C():
  inData = i2c.read_byte(I2C_ADD)
  return inData

prevI2CData = 0

def init():
	print("init and Clear the e-Paper HAT")
	initDisplay()
	

def checkForGimpy(distance):
	global LAST_STATUS, GIMPY_STATUS
	if distance < 0:
		print("gimpy is NOT FOUND", distance)		
		GIMPY_STATUS = 4
	elif distance < IS_CLOSE:
		print("gimpy is CLOSE ", distance)
		GIMPY_STATUS = 1
		
	elif distance > IS_FAR:
		print("gimpy is FAR ", distance)
		GIMPY_STATUS = 3
		
	elif distance >= IS_CLOSE and distance <= IS_FAR:
		print("gimpy is LURKING", distance)
		#GIMPY_STATUS = 2
		
	else:
		print("ERROR - Invalid Distance", distance)
	
	# Test if we need to update the screen due to status change. 
	# Also, Food denial happens here <TODO> Add food denial	
	if LAST_STATUS != GIMPY_STATUS:
		if GIMPY_STATUS == 1:
			writeI2C(180)
			draw(STATUS_1)
		elif GIMPY_STATUS == 2:
			print("ERROR - Gimpy Lurking Status Gone")
			#draw(STATUS_2 )
		elif GIMPY_STATUS == 3:
			writeI2C(0)
			draw(STATUS_3)
		elif GIMPY_STATUS == 4:
			writeI2C(0)
			draw(STATUS_4)
		else:
			print("ERROR - Invalid GIMPY_STATUS")
			#exit(-1)
		LAST_STATUS = GIMPY_STATUS
		

##############################
#			#
#	MAIN LOOP	#
#			#
##############################
i=0 # initializer for result testing logic
init()
while True:

	if readI2C(): # if the button push has happened
		print("Button Pushed")
		draw("Stopped: Allow Food")
		LAST_STATUS = 0
		while readI2C():
			time.sleep(1)
	
	
	SCAN_DURATION = 1.5
	LOOPS = 1
	# Declare the bluetooth scanner
	scanner = Scanner()
	devices = scanner.scan(SCAN_DURATION) # scan for X seconds. 

	# The world is full of bluetooth stuff. 
	# Check all found devices and pay attention to
	# the device attached to Gimpycat 
	for dev in devices:
		if str(dev.addr) == GIMPY:
			if DEBUG == True:
				print("Device %s (%s), RSSI=%d dB" % (dev.addr, dev.addrType, dev.rssi))
				for (adtype, desc, value) in dev.getScanData():
					print("  %s = %s" % (desc, value))
			# Add the measurement to the results list.
			raw_distances.append(dev.rssi)
	i = i + 1
	
	# Scan LOOPS times, then check results. check scan results
	if i % LOOPS == 0:
		# the best measurement is the smallest one.
		raw_distances.sort(reverse=True)
		if DEBUG == True:
			print("test for gimpy presence")
			print("Measurements: (%d)" % len(raw_distances), raw_distances[0])
		
		# the best measurement is the smallest one. 
		# sort the measurements, select the smallest, 
		# and make sure it is positive.
		if len(raw_distances) > 0 : 
			closest = abs(raw_distances[0])
			checkForGimpy(closest)
			# reset measurement results in prep for next test
			raw_distances.clear()
			
			NOT_fOUND_LOOPS=0
		else:
		
			NOT_fOUND_LOOPS-=1
			print("GIMPY NOT FOUND",NOT_fOUND_LOOPS)
			if NOT_fOUND_LOOPS < -20:
				checkForGimpy(NOT_fOUND_LOOPS)
			
				
	
